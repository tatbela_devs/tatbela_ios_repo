//
//  InputField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

protocol InputFieldDelegate: class {
    func iconButtenTapped(_ sender: UIButton)
    func phoneCodeButtonTapped(_ sender: UIButton)
    func myTextFieldShouldReturn(_ textField: UITextField) -> Bool
    func myTextFieldDidBeginEditing(_ textField: UITextField)
    func myTextFieldDidEndEditing(_ textField: UITextField)
}

// make all protocol functions optional
extension InputFieldDelegate where Self: InputField {
    func iconButtenTapped(_ sender: UIButton) {}
    func phoneCodeButtonTapped(_ sender: UIButton) {}
    func myTextFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func myTextFieldDidBeginEditing(_ textField: UITextField) {}
    func myTextFieldDidEndEditing(_ textField: UITextField) {}
}

@IBDesignable class InputField: UIView {
    
    // MARK: - Properties
    weak var delegate: InputFieldDelegate?
    var fieldValue: String { get { fatalError("fieldValue is not implemented") } }
    @IBInspectable var showLabelIcon: Bool = false
 
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet var label: UILabel!
    @IBOutlet var labelIcon: UIImageView!
    @IBOutlet var phoneCode: UIButton!
    @IBOutlet var textField: UITextField!
    @IBOutlet var icon: UIButton!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        textField.delegate = self
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "InputField", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    // MARK: - Actions
    @IBAction func iconTapped(_ sender: UIButton) {
        delegate?.iconButtenTapped(sender)
    }
    
    @IBAction func phoneCodeTapped(_ sender: UIButton) {
        delegate?.phoneCodeButtonTapped(sender)
    }
    
    // MARK: - Validation
    func validate() -> ValidationResult {
        fatalError("validate is not implemented")
    }
}

extension InputField: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return delegate?.myTextFieldShouldReturn(textField) ?? true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.myTextFieldDidBeginEditing(textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.myTextFieldDidEndEditing(textField)
    }
}
