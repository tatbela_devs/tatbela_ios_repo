//
//  AlmostDoneViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/12/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class AlmostDoneViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    
    // MARK: - View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor(named: Colors.Red3)
    }
    
    // MARK: - Actions
    @IBAction func getStart(_ sender: UIButton) {
        print("Get Start")
    }
}
