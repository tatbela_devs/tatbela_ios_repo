//
//  MapTableViewCell.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import GoogleMaps

class MapTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var mapView: GMSMapView!
    
    func initMap() {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 20.0)
        mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView
        print("init map")
    }

}
