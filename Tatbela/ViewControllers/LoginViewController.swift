//
//  LoginViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 5/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailField: EmailField!
    @IBOutlet weak var passwordField: PasswordField!
    @IBOutlet var inputFields: [InputField]!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        registerKeyboardNotifications()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
        unregisterKeyboardNotifications()
    }
    
    // MARK: - Methods
    private func validateFields() -> Bool {
        for inputField in inputFields {
            switch inputField.validate() {
            case .valid: break
            case .invalid(let errorMessage):
                showWarningAlert(errorMessage)
                return false
            }
        }
        return true
    }
    
    // MARK: - Keyboard Notification
    private func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardDidHide, object: nil)
    }
    private func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardDidHide, object: nil)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let kbSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
            let contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0)
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets: UIEdgeInsets = .zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    // MARK: - Actions
    @IBAction func login(_ sender: UIButton) {
        guard validateFields() else {
            return
        }
        
        HTTPRequest.login(
            email: emailField.fieldValue,
            password: passwordField.fieldValue,
            success: { [weak self] in
//                self?.showAlert(title: "", message: Message.LoginSuccess)
                self?.performSegue(withIdentifier: "ConfirmRegisteration", sender: nil)
            }, failure: { [weak self] in
                self?.showWarningAlert(Message.LoginFailed)
        })
    }
}
