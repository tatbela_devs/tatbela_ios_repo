//
//  HTTPRequest.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/5/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

typealias VoidBlock = () -> ()

class HTTPRequest {
    // these headers for testing
    static let defaultHeaders: [String: String] = [
        "Content-Type": "application/json"
    ]
    
    static func login(email: String, password: String, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        let parameters: [String: String] = [
            "email": email,
            "password": password
        ]
        HTTPHelper.HTTPPost(url: "users/login", parameters: parameters, headers: defaultHeaders, success: { result in
            print(result)
            success()
        }) { error in
            print(error)
            failure()
        }
    }
    
    static func forgetPassword(email: String, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        let parameters: [String: String] = [
            "email": email
        ]
        HTTPHelper.HTTPPost(url: "users/forget-password", parameters: parameters, headers: defaultHeaders, success: { result in
            print(result)
            success()
        }) { error in
            print(error)
            failure()
        }
    }
    
    static func register(_ model: RegisterationModel, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        HTTPHelper.HTTPPost(url: "users/register", parameters: model.toJSON(), headers: defaultHeaders, success: { result in
            print(result)
            success()
        }) { error in
            print(error)
            failure()
        }
    }
    
    static func confirmPin(_ pin: String, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        let parameters = [
            "pin": pin
        ]
        HTTPHelper.HTTPPost(url: "users/confirm-pin", parameters: parameters, headers: defaultHeaders, success: { result in
            print(result)
            success()
        }) { error in
            print(error)
            failure()
        }
    }
    
    static func resendPin(success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        HTTPHelper.HTTPGet(url: "users/resend-pin", parameters: nil, headers: defaultHeaders, success: { result in
            print(result)
            success()
        }) { error in
            print(error)
            failure()
        }
    }
}
