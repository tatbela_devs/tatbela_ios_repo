//
//  BoolExtension.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

extension Bool {
    mutating func toggle() {
        self = !self
    }
}
