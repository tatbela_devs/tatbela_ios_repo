//
//  EmailField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class EmailField: InputField {
    
    override var showLabelIcon: Bool { didSet { setupLabelIcon() } }
    
    override var fieldValue: String {
        return textField.text ?? ""
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        delegate = self
        setupLabel()
        setupLabelIcon()
        setupPhoneCodeButton()
        setupTextField()
        setupIcon()
    }
    
    private func setupLabel() {
        label.text = "Email"
    }
    
    private func setupLabelIcon() {
        labelIcon.isHidden = !showLabelIcon
        let bundle = Bundle(for: type(of: self))
        labelIcon.image = UIImage(named: "emailIcon", in: bundle, compatibleWith: traitCollection)
    }
    
    private func setupPhoneCodeButton() {
        phoneCode.isHidden = true
    }
    
    private func setupTextField() {
        textField.placeholder = "name@mail.com"
        textField.textContentType = .emailAddress
    }
    
    private func setupIcon() {
        let bundle = Bundle(for: type(of: self))
        let image = UIImage(named: "XIcon", in: bundle, compatibleWith: traitCollection)
        icon.setImage(image, for: .normal)
        icon.tintColor = UIColor(named: Colors.Grey2, in: bundle, compatibleWith: traitCollection)
    }
    
    // MARK: - Validation
    override func validate() -> ValidationResult {
        // validate required/not empty
        guard Validation.validateEmptyString(textField.text) else {
            return .invalid(errorMessage: ValidationMessage.EmptyEmail)
        }
        // validate email format
        guard Validation.validateEmail(textField.text) else {
            return .invalid(errorMessage: ValidationMessage.InvalidEmail)
        }
        return .valid
    }
}

extension EmailField: InputFieldDelegate {
    func iconButtenTapped(_ sender: UIButton) {
        textField.text = nil
    }
}
