//
//  Constants.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/3/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

class Message {
    static let RegisterSuccess = "Registered Successfully"
    static let RegisterFailed = "Registeration Failed"
    static let LoginSuccess = "Login Successfully"
    static let LoginFailed = "Invalid email or password"
    static let ResetPasswordSuccess = "Password reset successfully"
    static let ResetPasswordFailed = "Email not found"
    static let ConfirmPINSuccess = "PIN confirmed successfully"
    static let ConfirmPINFailed = "PIN confirmation failed"
    static let ResendPINSuccess = "PIN Resent successfully"
    static let ResendPINFailed = "PIN resend failed"
}

class LeftMenu {
    static let items: [(icon: String, title: String)] = [
        ("i", "Home"),
        ("i", "Our Chiefs"),
        ("i", "All Meals"),
        ("i", "My Profile"),
        ("i", "My Addresses"),
        ("i", "My Orders"),
        ("i", "Customer Support"),
        ("i", "Notifications"),
        ("i", "Contact Us"),
        ("i", "About us"),
        ("i", "Logout")
    ]
}
