//
//  RoundedTextField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class RoundedTextField: UITextField {
    
    @IBInspectable var borderColor: UIColor = UIColor(named: Colors.Grey2) ?? .clear { didSet { setNeedsLayout() } }
    let borderWidth: CGFloat = 1
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 13
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
}
