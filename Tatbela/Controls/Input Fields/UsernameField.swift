//
//  UsernameField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//


import UIKit

@IBDesignable class UsernameField: InputField {
    
    override var fieldValue: String {
        return textField.text ?? ""
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        delegate = self
        setupLabel()
        setupLabelIcon()
        setupPhoneCodeButton()
        setupTextField()
        setupIcon()
    }
    
    private func setupLabel() {
        label.text = "User Name"
    }
    
    private func setupLabelIcon() {
        labelIcon.isHidden = true
    }
    
    private func setupPhoneCodeButton() {
        phoneCode.isHidden = true
    }
    
    private func setupTextField() {
        textField.placeholder = "username"
        textField.textContentType = .username
    }
    
    private func setupIcon() {
        let bundle = Bundle(for: type(of: self))
        let image = UIImage(named: "TIcon", in: bundle, compatibleWith: traitCollection)
        icon.setImage(image, for: .normal)
        icon.tintColor = UIColor(named: Colors.Green1, in: bundle, compatibleWith: traitCollection)
    }
    
    // MARK: - Validation
    override func validate() -> ValidationResult {
        // validate required/not empty
        guard Validation.validateEmptyString(textField.text) else {
            return .invalid(errorMessage: ValidationMessage.EmptyUsername)
        }
        return .valid
    }
}

extension UsernameField: InputFieldDelegate { }
