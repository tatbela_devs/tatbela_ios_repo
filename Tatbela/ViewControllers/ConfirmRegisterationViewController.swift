//
//  ConfirmRegisterationViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class ConfirmRegisterationViewController: UIViewController {
    
    // MARK: - Properties
    var pin: String {
        guard let p1 = tf1.text, let p2 = tf2.text, let p3 = tf3.text, let p4 = tf4.text else {
            return ""
        }
        return "\(p1)\(p2)\(p3)\(p4)"
    }
    
    // MARK: - Outlets
    @IBOutlet weak var tf1: RoundedTextField!
    @IBOutlet weak var tf2: RoundedTextField!
    @IBOutlet weak var tf3: RoundedTextField!
    @IBOutlet weak var tf4: RoundedTextField!
    @IBOutlet var textFields: [UITextField]!
    
    // MARK: - View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor(named: Colors.Yellow3)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTextFields()
    }
    
    // MARK: - Methods
    private func initTextFields() {
        tf1.delegate = self
        tf2.delegate = self
        tf3.delegate = self
        tf4.delegate = self
        
        tf1.clearsOnInsertion = true
        tf2.clearsOnInsertion = true
        tf3.clearsOnInsertion = true
        tf4.clearsOnInsertion = true
    }
    
    private func validateFields() -> Bool {
        for tf in textFields {
            if !Validation.validateStringLength(tf.text, min: 1, max: 1) {
                showWarningAlert(ValidationMessage.EmptyPIN)
                return false
            }
        }
        
        return true
    }
    
    // MARK: - Actions
    @IBAction func textChanged(_ sender: UITextField) {
        if let text = sender.text, text.count >= 1 {
            switch sender {
            case tf1: tf2.becomeFirstResponder()
            case tf2: tf3.becomeFirstResponder()
            case tf3: tf4.becomeFirstResponder()
            default: break
            }
        }
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        guard validateFields() else {
            return
        }
        
        HTTPRequest.confirmPin(pin, success: { [weak self] in
//            self?.showAlert(title: "", message: Message.ConfirmPINSuccess)
            self?.performSegue(withIdentifier: "AlmostDone", sender: nil)
        }, failure: { [weak self] in
            self?.showWarningAlert(Message.ConfirmPINFailed)
        })
    }
    
    @IBAction func resendConfirmationCode(_ sender: UIButton) {
        HTTPRequest.resendPin(success: { [weak self] in
            self?.showAlert(title: "", message: Message.ResendPINSuccess)
            }, failure: { [weak self] in
                self?.showWarningAlert(Message.ResendPINFailed)
        })
    }
}

extension ConfirmRegisterationViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // only 1 character allowed in each text field
        if string.count > 1 || range.location > 0 {
            return false
        } else {
            return true
        }
    }
}
