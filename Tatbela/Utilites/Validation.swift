//
//  Validation.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/5/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class Validation {
    static func validateEmptyString(_ string: String?) -> Bool {
        guard let string = string else {
            return false
        }
        return string.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ? false : true
    }
    
    static func validateStringLength(_ string: String?, min: Int, max: Int) -> Bool {
        guard validateEmptyString(string) else {
            return false
        }
        
        return string!.count >= min && string!.count <= max
    }
    
    private static func validateStringWithExepression(_ string: String, exepression regEx: String) -> Bool {
        let test = NSPredicate(format: "SELF MATCHES %@", regEx)
        return test.evaluate(with: string)
    }
    
    static func validateEmail(_ email: String?) -> Bool {
        guard let email = email else {
            return false
        }
        let emailRegEx = "^[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}$"
        return validateStringWithExepression(email, exepression: emailRegEx)
    }
    
    static func validatePhoneNumber(_ phoneNumber: String?) -> Bool {
        guard let phoneNumber = phoneNumber else {
            return false
        }
        let phoneRegEx = "^[0-9]{5,12}$"
        return validateStringWithExepression(phoneNumber, exepression: phoneRegEx)
    }
}

enum ValidationResult {
    case valid
    case invalid(errorMessage: String)
}


class ValidationMessage {
    static let EmptyUsername = "Please enter your username"
    static let InvalidUsername = "Invalid username"
    static let EmptyEmail = "Please enter your email"
    static let InvalidEmail = "Please enter a valid email"
    static let EmptyPassword = "Please enter your password"
    static let EmptyPhoneNumber = "Please enter your phone number"
    static let EmptyCountryCode = "Please select your country code"
    static let InvalidPhoneNumber = "Please enter a valid phone number"
    static let EmptyPIN = "Please enter PIN"
    static let InvalidPIN = "Invalid PIN"
}
