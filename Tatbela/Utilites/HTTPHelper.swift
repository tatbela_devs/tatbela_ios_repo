//
//  HTTPHelper.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/5/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

class HTTPHelper {
    static let baseURL: String = "https://aa229c93-e480-4f3b-9dfc-b465e46035be.mock.pstmn.io"
    
    static func HTTPGet(url: String, parameters: Parameters?, headers: HTTPHeaders?, success: @escaping (Any) -> (), failure: @escaping (Error) -> ()) {
        SVProgressHUD.showFull()
        Alamofire.request("\(baseURL)/\(url)", method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: headers).validate().responseJSON { response in
            SVProgressHUD.dismiss(completion: {
                switch response.result {
                case .success:
                    success(response)
                case .failure(let error):
                    failure(error)
                }
            })
        }
    }
    
    static func HTTPPost(url: String, parameters: Parameters?, headers: HTTPHeaders?, success: @escaping (Any) -> (), failure: @escaping (Error) -> ()) {
        SVProgressHUD.showFull()
        Alamofire.request("\(baseURL)/\(url)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
            SVProgressHUD.dismiss(completion: {
                switch response.result {
                case .success:
                    success(response)
                case .failure(let error):
                    failure(error)
                }
            })
        }
    }
}
