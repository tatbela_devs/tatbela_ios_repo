//
//  RoundedButton.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/9/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class RoundedButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
        clipsToBounds = true
    }
}
