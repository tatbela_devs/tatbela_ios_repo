//
//  LeftMenuViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/3/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
    }

}

extension LeftMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 0 : LeftMenu.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            // TODO: not implemented yet
            let cell = UITableViewCell()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuItem") as! LeftMenuItemTableViewCell
            cell.iconLabel.text = LeftMenu.items[indexPath.row].icon
            cell.titleLabel.text = LeftMenu.items[indexPath.row].title
            return cell
        }
    }
    
}
