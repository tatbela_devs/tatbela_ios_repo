//
//  Meal.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

class Meal {
    var name: String?
    var category: String?
    var desc: String?
    var price: Double?
    var imageUrl: String?
}
