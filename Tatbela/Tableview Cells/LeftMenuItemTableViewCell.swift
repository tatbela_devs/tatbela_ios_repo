//
//  LeftMenuItemTableViewCell.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/3/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class LeftMenuItemTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
}
