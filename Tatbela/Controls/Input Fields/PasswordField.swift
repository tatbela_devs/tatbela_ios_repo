//
//  PasswordField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class PasswordField: InputField {
    
    override var showLabelIcon: Bool { didSet { setupLabelIcon() } }
    
    override var fieldValue: String {
        return textField.text ?? ""
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        delegate = self
        setupLabel()
        setupLabelIcon()
        setupPhoneCodeButton()
        setupTextField()
        setupIcon()
    }
    
    private func setupLabel() {
        label.text = "Password"
    }
    
    private func setupLabelIcon() {
        labelIcon.isHidden = !showLabelIcon
        let bundle = Bundle(for: type(of: self))
        labelIcon.image = UIImage(named: "passwordIcon", in: bundle, compatibleWith: traitCollection)
    }
    
    private func setupPhoneCodeButton() {
        phoneCode.isHidden = true
    }
    
    private func setupTextField() {
        textField.placeholder = "Your passcode"
        textField.isSecureTextEntry = true
        textField.textContentType = .password
    }
    
    private func setupIcon() {
        let bundle = Bundle(for: type(of: self))
        let image = UIImage(named: "IIcon", in: bundle, compatibleWith: traitCollection)
        icon.setImage(image, for: .normal)
        icon.tintColor = UIColor(named: Colors.Grey2, in: bundle, compatibleWith: traitCollection)
    }
    
    // MARK: - Validation
    override func validate() -> ValidationResult {
        // validate required/not empty
        guard Validation.validateEmptyString(textField.text) else {
            return .invalid(errorMessage: ValidationMessage.EmptyPassword)
        }
        return .valid
    }
}

extension PasswordField: InputFieldDelegate {
    func iconButtenTapped(_ sender: UIButton) {
        textField.isSecureTextEntry.toggle()
    }
}
