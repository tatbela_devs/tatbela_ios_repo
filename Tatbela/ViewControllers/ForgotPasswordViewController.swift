//
//  ForgotPasswordViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 5/31/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailField: EmailField!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor(named: Colors.Green3)
        registerKeyboardNotifications()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterKeyboardNotifications()
    }
    
    // MARK: - Methods    
    private func validateFields() -> Bool {
        switch emailField.validate() {
        case .valid: break
        case .invalid(let errorMessage):
            showWarningAlert(errorMessage)
            return false
        }
        return true
    }
    
    // MARK: - Keyboard Notification
    private func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardDidHide, object: nil)
    }
    private func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardDidHide, object: nil)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let kbSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
            let contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0)
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets: UIEdgeInsets = .zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    // MARK: - Actions
    @IBAction func resetPassword(_ sender: UIButton) {
        guard validateFields() else {
            return
        }
        
        HTTPRequest.forgetPassword(
            email: emailField.fieldValue,
            success: { [weak self] in
                self?.showAlert(title: "", message: Message.ResetPasswordSuccess)
            }, failure: { [weak self] in
                self?.showWarningAlert(Message.ResetPasswordFailed)
        })
    }
}
