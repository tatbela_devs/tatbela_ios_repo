//
//  HomeTableViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class HomeTableViewController: UITableViewController {

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        print("hello")
    }
    
    // MARK: - Methods
    private func initTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        // hide extra cells
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    // MARK: - TableView Delegate & Data Source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MapCell", for: indexPath) as! MapTableViewCell
            cell.initMap()
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChiefCell", for: indexPath) as! ChiefTableViewCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MealCell", for: indexPath) as! MealTableViewCell
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 2 {
            return "Today's Menu"
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 300
        } else if indexPath.section == 1 {
            return 100
        } else {
            return 145
        }
    }

}
