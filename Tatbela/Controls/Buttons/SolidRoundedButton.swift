//
//  SolidRoundedButton.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class SolidRoundedButton: RoundedButton {
    
    @IBInspectable var borderColor: UIColor = UIColor(named: Colors.Green1) ?? .clear { didSet { setNeedsLayout() } }
    @IBInspectable var fillColor: UIColor = .white { didSet { setNeedsLayout() } }
    let borderWidth: CGFloat = 1
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.backgroundColor = fillColor.cgColor
    }
}
