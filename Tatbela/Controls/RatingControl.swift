//
//  RatingControl.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class RatingControl: UIView {
    
    // MARK: - Properties
    var rate: Int = 0 { didSet { rateChanged() } }
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet var ratingButtons: [UIButton]!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "RatingControl", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    // MARK: - Methods
    private func rateChanged() {
        for button in ratingButtons {
            button.isSelected = button.tag <= rate
        }
    }
    
    // MARK: - Actions
    @IBAction func setRate(_ sender: UIButton) {
        rate = sender.tag
    }
    
    
}
