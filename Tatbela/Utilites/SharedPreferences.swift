//
//  SharedPreferences.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 5/31/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class SharedPreferences {
    static let userDefaults = UserDefaults.standard
    static var isRTL = false
    
    static func changeLanguage() {
        isRTL = !isRTL
        
        if(isRTL) {
            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            UITextView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            UITextField.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            UILabel.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            UISearchBar.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            UITextView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            UITextField.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            UILabel.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
        }
    }
}
