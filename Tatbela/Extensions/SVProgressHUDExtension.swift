//
//  SVProgressHUDExtension.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/12/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import SVProgressHUD

extension SVProgressHUD {
    class func showFull() {
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
    }
}
