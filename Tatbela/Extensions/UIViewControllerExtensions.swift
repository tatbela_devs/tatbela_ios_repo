//
//  UIViewControllerExtensions.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/6/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showWarningAlert(_ message: String) {
        showAlert(title: "Warning", message: message)
    }
}
