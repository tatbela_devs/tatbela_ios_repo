//
//  Colors.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

class Colors {
    static var Green1 = "Green1"
    static var Green2 = "Green2"
    static var Green3 = "Green3"
    static var Grey1 = "Grey1"
    static var Grey2 = "Grey2"
    static var Yellow1 = "Yellow1"
    static var Yellow2 = "Yellow2"
    static var Yellow3 = "Yellow3"
    static var Red1 = "Red1"
    static var Red2 = "Red2"
    static var Red3 = "Red3"
}
